package org.example.model;

public class Cat {
    private String name; // Имя
    private String color; // Цвет
    private boolean isthoroughbred; // Породистый
    private int age; // Возраст
    private int prettiness; // Красивость
    private String meow; // Голос кота
    private int weight; // Вес
    private double satiety; // Сытость
    private static int count = 0;

    public Cat() {
        count++;
    }

    public Cat(String name,
               String color,
               boolean isThoroughbred,
               int age, int prettiness,
               String meow, int weight,
               double satiety) {
        setName(name);
        setColor(color);
        setIsThoroughbred(isThoroughbred);
        setAge(age);
        setPrettiness(prettiness);
        setMeow(meow);
        setWeight(weight);
        setSatiety(satiety);
        count++;
    }

    public void setCat(String name,
                       String color,
                       boolean isThoroughbred,
                       int age, int prettiness,
                       String meow, int weight,
                       double satiety) {
        setName(name);
        setColor(color);
        setIsThoroughbred(isThoroughbred);
        setAge(age);
        setPrettiness(prettiness);
        setMeow(meow);
        setWeight(weight);
        setSatiety(satiety);
    }

    public void displayInfo() {
        System.out.printf("Name: %s \t " +
                        "Color: %s \t " +
                        "IsThoroughbred: %b \t" +
                        " Age: %d \t" +
                        "Prettiness: %d \t" +
                        " Meow: %s \t " +
                        "Weight: %d \t" +
                        " Satiety: %f \n",
                this.name,
                this.color,
                this.isthoroughbred,
                this.age,
                this.prettiness,
                this.meow,
                this.weight,
                this.satiety);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean IsThoroughbred() {
        return this.isthoroughbred;
    }

    public void setIsThoroughbred(boolean thoroughbred) {
        this.isthoroughbred = thoroughbred;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPrettiness() {
        return this.prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getMeow() {
        return this.meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getSatiety() {
        return this.satiety;
    }

    public void setSatiety(double satiety) {
        this.satiety = satiety;
    }

    public static int getCount() {
        return count;
    }

    public void eat(double satiety) {
        this.satiety += satiety;
        System.out.println("Кот покормлен, текущая сытость: " + this.satiety);
    }

    public void eat(double satiety, String food) {
        // Здесь только количество условных единиц сытости и название еды
        this.satiety += satiety;
        System.out.printf("Покормили едой \"%s\", текущая сытость: %f \n", food, this.satiety);
    }

    public void eat() {
        // Здесь вызов перегрузки с названием еды и сытости
        String[] Food = {"Колбаса", "Рыба", "Консервы", "Сухой корм"};
        int index_food = (int) (Math.random() * Food.length);
        eat(0.55, Food[index_food]);
    }

    public boolean play() {
        if (this.satiety > 0) {
            this.satiety -= 0.13;
            System.out.println("Я играю " + this.satiety);
            return true;
        }
        System.out.print("Я не буду играть, покорми меня! ");
        return false;
    }

    public boolean sleep() {
        if (this.satiety > 0) {
            this.satiety -= 0.09;
            System.out.println("Я сплю " + this.satiety);
            return true;
        }
        System.out.print("Я не буду спать, покорми меня! ");
        return false;
    }

    public boolean chaseMouse() {
        if (this.satiety > 0) {
            this.satiety -= 0.18;
            System.out.println("Я ловлю мышей " + this.satiety);
            return true;
        }
        System.out.print("Я не буду ловить мышей, покорми меня! ");
        return false;
    }

    public boolean jump() {
        if (this.satiety > 0) {
            this.satiety -= 0.12;
            System.out.println("Я прыгаю " + this.satiety);
            return true;
        }
        System.out.print("Я не буду прыгать, покорми меня! ");
        return false;
    }

    public boolean wash() {
        if (this.satiety > 0) {
            this.satiety -= 0.09;
            System.out.println("Я умываюсь " + this.satiety);
            return true;
        }
        System.out.print("Я не буду умываться, покорми меня! ");
        return false;
    }

    private void doAction(boolean action) {
        if (!action) {
            eat();
        }
    }

    public static boolean isMeowEquals(Cat cat1, Cat cat2) {
        return cat1.getMeow().equals(cat2.getMeow());
    }

    public void liveAnotherDay() {
        for (int i = 1; i < 25; i++) {
            int index_action = (int) (Math.random() * 5);
            System.out.print(i + ". ");
            switch (index_action) {
                case 0:
                    doAction(play());
                    break;
                case 1:
                    doAction(sleep());
                    break;
                case 2:
                    doAction(chaseMouse());
                    break;
                case 3:
                    doAction(jump());
                    break;
                case 4:
                    doAction(wash());
                    break;
            }
        }
    }
}