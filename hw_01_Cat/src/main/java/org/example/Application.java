package org.example;

import org.example.model.Cat;

public class Application {
    public static void main(String[] args) {
        // Задание исходных параметров для рандомизации
        String[] Name = {"Том", "Гарфилд", "Локи", "Багира", "Соня", "Ника"};
        String[] Color = {"Черный", "Белый", "Рыжий", "Серый"};
        boolean[] IsThoroughbred = {true, false};
        String[] Meow = {"Мяу", "Мя-я-я-у", "Мя", "Миу"};
        Cat cat1 = new Cat(Name[(int) (Math.random() * Name.length)],
                Color[(int) (Math.random() * Color.length)],
                IsThoroughbred[(int) (Math.random() * IsThoroughbred.length)],
                (int) (Math.random() * 18),
                (int) (Math.random() * 10),
                Meow[(int) (Math.random() * Meow.length)],
                (int) (Math.random() * 6),
                Math.random());
        cat1.displayInfo();
        cat1.liveAnotherDay();
        System.out.println();
        // Имя - Цвет - Породистый - Возраст - Красивость - Голос - Вес - Сытость
        Cat cat2 = new Cat();
        cat2.setCat(Name[(int) (Math.random() * Name.length)],
                Color[(int) (Math.random() * Color.length)],
                IsThoroughbred[(int) (Math.random() * IsThoroughbred.length)],
                (int) (Math.random() * 18),
                (int) (Math.random() * 10),
                Meow[(int) (Math.random() * Meow.length)],
                (int) (Math.random() * 6),
                Math.random());
        cat2.displayInfo();
        cat2.liveAnotherDay();
        System.out.println("Коты говорят одинаково? " + Cat.isMeowEquals(cat1, cat2));
        System.out.println("Количество созданных котов: " + Cat.getCount());
    }
}