package org.example;

import org.example.Food.*;
import org.example.animals.Bear;
import org.example.animals.Duck;
import org.example.animals.Fish;
import org.example.animals.Koala;
import org.example.animals.Lama;
import org.example.animals.Swim;
import org.example.animals.Wolf;

public class Zoo {
    public static void main(String[] args) {
        Bear bear = new Bear("Медведь",
                200,
                true,
                "Бурый",
                "Млекопитающее",
                true,
                Math.random());
        Duck duck = new Duck("Утка",
                2,
                true,
                "Белый",
                "Птица",
                true,
                Math.random());
        Fish fish = new Fish("Рыба",
                1.1, false,
                "Серый",
                "Водоплавающее",
                false,
                Math.random());
        Koala koala = new Koala("Коала",
                2.5,
                true,
                "Серый",
                "Млекопитающее",
                true,
                Math.random());
        Lama lama = new Lama("Лама",
                50,
                true,
                "Золотой",
                "Млекопитающее",
                false, Math.random());
        Wolf wolf = new Wolf("Волк",
                42,
                true,
                "Черный",
                "Млекопитающее",
                true,
                Math.random());

        Cereals cereals = new Cereals("Крупа",
                1.1,
                140,
                0.3,
                true);

        Oats oats = new Oats("Овес",
                4.1,
                143,
                0.4,
                false);

        Sardines sardines = new Sardines("Сардины",
                3.2,
                157,
                0.6,
                true);

        Steak steak = new Steak("Стейк",
                5.2,
                461,
                0.54,
                true);

        Straw straw = new Straw("Солома",
                1.5,
                200,
                0.2,
                true);

        Veal veal = new Veal("Телятина",
                6.0,
                307,
                0.7,
                true);

        Worker worker = new Worker("Worker",
                42,
                true);


        worker.feed(bear, veal);
        worker.feed(bear, straw);

        worker.feed(duck, steak);
        worker.feed(duck, oats);

        worker.getVoice(bear);
        worker.getVoice(duck);
        worker.getVoice(wolf);

        Swim[] pool = {bear, duck, fish, wolf};
        for (Swim animal :
                pool) {
            animal.swim();
        }
    }
}
