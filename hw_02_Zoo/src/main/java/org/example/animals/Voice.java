package org.example.animals;

public interface Voice {
    String voice();
}
