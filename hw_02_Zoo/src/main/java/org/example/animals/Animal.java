package org.example.animals;

import org.example.Food.Food;

public abstract class Animal {
    private String name;
    private double weight;
    private boolean isWool;
    private String color;
    private String kind;
    private double satiety;

    public Animal(String name, double weight, boolean isWool, String color, String kind, double satiety) {
        setName(name);
        setWeight(weight);
        setIsWool(isWool);
        setColor(color);
        setKind(kind);
        setSatiety(satiety);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean getIsWool() {
        return this.isWool;
    }

    public void setIsWool(boolean isWool) {
        this.isWool = isWool;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public double getSatiety() {
        return this.satiety;
    }

    public void setSatiety(double satiety) {
        this.satiety = satiety;
    }

    abstract public void eat(Food food);
}
