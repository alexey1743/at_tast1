package org.example.animals;

public class Lama extends Herbivore implements Run {
    private boolean fast;

    public Lama(String name,
                double weight,
                boolean isWool,
                String color,
                String kind,
                boolean fast,
                double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setFast(fast);
    }

    public boolean getFast() {
        return this.fast;
    }

    public void setFast(boolean shy) {
        this.fast = fast;
    }

    public void run() {
        System.out.println("Я убегаю от хищников");
    }

}
