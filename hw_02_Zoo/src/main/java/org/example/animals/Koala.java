package org.example.animals;

public class Koala extends Herbivore implements Run {
    // Пугливый?
    private boolean shy;

    public Koala(String name,
                 double weight,
                 boolean isWool,
                 String color,
                 String kind,
                 boolean shy,
                 double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setShy(shy);
    }

    public boolean getShy() {
        return this.shy;
    }

    public void setShy(boolean shy) {
        this.shy = shy;
    }

    public void run() {
        System.out.println("Я могу бегать по деревьям");
    }
}
