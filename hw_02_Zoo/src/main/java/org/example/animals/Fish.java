package org.example.animals;

public class Fish extends Carnivorous implements Swim {
    // Чешуйчатый?
    private boolean scaly;

    public Fish(String name,
                double weight,
                boolean isWool,
                String color,
                String kind,
                boolean scaly,
                double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setScaly(scaly);
    }

    public boolean getScaly() {
        return this.scaly;
    }

    public void setScaly(boolean scaly) {
        this.scaly = scaly;
    }

    public void swim() {
        System.out.println("Я очень быстро плаваю");
    }

}
