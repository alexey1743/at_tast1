package org.example.animals;

public class Wolf extends Carnivorous implements Run, Swim, Voice {
    // Одиночка?
    private boolean loner;

    public Wolf(String name,
                double weight,
                boolean isWool,
                String color,
                String kind,
                boolean loner,
                double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setLoner(loner);
    }

    public boolean getLoner() {
        return this.loner;
    }

    public void setLoner(boolean loner) {
        this.loner = loner;
    }

    public void run() {
        System.out.println("Я могу очень быстро бежать");
    }

    public void swim() {
        System.out.println("Если я волк, значит плаваю как собака");
    }

    public String voice() {
        return "У-у-у";
    }

}
