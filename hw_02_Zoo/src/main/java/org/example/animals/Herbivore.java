package org.example.animals;

import org.example.Food.Food;
import org.example.Food.Grass;

abstract class Herbivore extends Animal {
    public Herbivore(String name, double weight, boolean isWool, String color, String kind, double satiety) {
        super(name, weight, isWool, color, kind, satiety);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            System.out.println("Кушать можно");
            double difference = Math.random();
            setSatiety(getSatiety() + difference);
            System.out.println("Повысиласть сытость на " + difference);
        } else {
            System.out.println("Кушать нельзя");
        }
    }
}
