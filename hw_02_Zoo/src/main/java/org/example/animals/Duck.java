package org.example.animals;

public class Duck extends Herbivore implements Swim, Voice, Fly, Run {
    // Дикая?
    private boolean wild;

    public Duck(String name,
                double weight,
                boolean isWool,
                String color,
                String kind,
                boolean wild,
                double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setWild(wild);
    }

    public boolean getWild() {
        return this.wild;
    }

    public void setWild(boolean shy) {
        this.wild = wild;
    }

    public void swim() {
        System.out.println("Я плаваю");
    }

    public String voice() {
        return "Кря-я-я";
    }

    public void fly() {
        System.out.println("Я летаю");
    }

    public void run() {
        System.out.println("Я медленно бегу");
    }
}
