package org.example.animals;

import org.example.Food.Food;

public class Bear extends Carnivorous implements Run, Swim, Voice {
    // Арктический?
    private boolean isArctic;

    public Bear(String name,
                double weight,
                boolean isWool,
                String color,
                String kind,
                boolean isArctic,
                double satiety) {
        super(name, weight, isWool, color, kind, satiety);
        setIsArctic(isArctic);
    }

    public boolean getIsArctic() {
        return this.isArctic;
    }

    public void setIsArctic(boolean isArctic) {
        this.isArctic = isArctic;
    }

    public void run() {
        System.out.println("Я медленно бегаю");
        setSatiety(getSatiety() - 0.1);
    }

    public void swim() {
        System.out.println("Я могу очень глубоко нырять");
        setSatiety(getSatiety() - 0.15);
    }

    public String voice() {
        return "Ры-ры-ры";
    }

    @Override
    public void eat(Food food) {
        super.eat(food);
    }

}
