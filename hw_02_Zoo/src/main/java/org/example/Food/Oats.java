package org.example.Food;

public class Oats extends Grass {
    private boolean isTopGrade;

    public Oats(String name, double weight, int calories, double proportion_of_carbohydrates, boolean isTopGrade) {
        super(name, weight, calories, proportion_of_carbohydrates);
        setTopGrade(isTopGrade);

    }

    public void setTopGrade(boolean topGrade) {
        this.isTopGrade = topGrade;
    }

    public boolean getTopGrade() {
        return this.isTopGrade;
    }
}
