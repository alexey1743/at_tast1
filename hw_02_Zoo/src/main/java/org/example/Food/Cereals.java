package org.example.Food;

public class Cereals extends Grass {
    private boolean isDelicious;

    public Cereals(String name, double weight, int calories, double proportion_of_carbohydrates, boolean delicious) {
        super(name, weight, calories, proportion_of_carbohydrates);
        setDelicious(delicious);
    }

    public boolean getIsDelicious() {
        return this.isDelicious;
    }

    public void setDelicious(boolean delicious) {
        this.isDelicious = delicious;
    }
}
