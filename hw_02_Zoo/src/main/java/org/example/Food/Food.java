package org.example.Food;

public abstract class Food {
    private String name; // Название еды
    private double weight; // Вес
    private int calories; // Калорийность

    public Food(String name, double weight, int calories) {
        setName(name);
        setWeight(weight);
        setCalories(calories);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCalories() {
        return this.calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }
}
