package org.example.Food;

public class Steak extends Meat {
    private boolean isRaw;

    public Steak(String name, double weight, int calories, double proportion_of_fat, boolean isRaw) {
        super(name, weight, calories, proportion_of_fat);
        setIsRaw(isRaw);
    }

    public boolean getIsRaw() {
        return this.isRaw;
    }

    public void setIsRaw(boolean raw) {
        this.isRaw = raw;
    }
}
