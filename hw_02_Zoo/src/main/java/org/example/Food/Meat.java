package org.example.Food;

public abstract class Meat extends Food {
    // Доля жиров в мясе
    private double proportion_of_fat;

    public Meat(String name, double weight, int calories, double proportion_of_fat) {
        super(name, weight, calories);
        setProportion_of_fat(proportion_of_fat);
    }

    public double getProportion_of_fat() {
        return this.proportion_of_fat;
    }

    public void setProportion_of_fat(double proportion_of_fat) {
        this.proportion_of_fat = proportion_of_fat;
    }
}
