package org.example.Food;

public class Sardines extends Meat {
    private boolean isBone;

    public Sardines(String name, double weight, int calories, double proportion_of_fat, boolean isBone) {
        super(name, weight, calories, proportion_of_fat);
        setIsBone(isBone);
    }

    public boolean getIsBone() {
        return this.isBone;
    }

    public void setIsBone(boolean bone) {
        this.isBone = bone;
    }
}
