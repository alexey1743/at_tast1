package org.example.Food;

public class Straw extends Grass {
    private boolean isDry;

    public Straw(String name, double weight, int calories, double proportion_of_carbohydrates, boolean isDry) {
        super(name, weight, calories, proportion_of_carbohydrates);
        setIsDry(isDry);

    }

    public boolean getIsDry() {
        return this.isDry;
    }

    public void setIsDry(boolean isDry) {
        this.isDry = isDry;
    }

}
