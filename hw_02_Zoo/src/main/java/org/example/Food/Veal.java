package org.example.Food;

public class Veal extends Meat {
    private boolean fresh;

    public Veal(String name, double weight, int calories, double proportion_of_fat, boolean fresh) {
        super(name, weight, calories, proportion_of_fat);
        setIsFresh(fresh);
    }

    public boolean getIsFresh() {
        return this.fresh;
    }

    public void setIsFresh(boolean fresh) {
        this.fresh = fresh;
    }
}
