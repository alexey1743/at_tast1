package org.example.Food;

public abstract class Grass extends Food {
    // Доля углеводов в еде травоядных
    private double proportion_of_carbohydrates;

    public Grass(String name, double weight, int calories, double proportion_of_carbohydrates) {
        super(name, weight, calories);
        setProportion_of_carbohydrates(proportion_of_carbohydrates);
    }

    public double getProportion_of_carbohydrates() {
        return this.proportion_of_carbohydrates;
    }

    public void setProportion_of_carbohydrates(double proportion_of_carbohydrates) {
        this.proportion_of_carbohydrates = proportion_of_carbohydrates;
    }
}
