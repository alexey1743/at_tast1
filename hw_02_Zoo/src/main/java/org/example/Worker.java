package org.example;

import org.example.Food.Food;
import org.example.animals.Animal;
import org.example.animals.Voice;

public class Worker {
    private String name;
    private int age;
    private boolean skilled;

    public Worker(String name, int age, boolean skilled) {
        setName(name);
        setAge(age);
        setSkilled(skilled);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getSkilled() {
        return this.skilled;
    }

    public void setSkilled(boolean skilled) {
        this.skilled = skilled;
    }

    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

}
